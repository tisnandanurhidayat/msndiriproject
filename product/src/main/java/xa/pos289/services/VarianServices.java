package xa.pos289.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xa.pos289.models.Varian;
import xa.pos289.repositories.VarianRepo;

@Service
@Transactional
public class VarianServices {
	
	@Autowired VarianRepo varianrepo;
	
	public List<Varian> listAll(){
		return this.varianrepo.findAll();
	}

}
