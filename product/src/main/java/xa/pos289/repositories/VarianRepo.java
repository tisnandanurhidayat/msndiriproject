package xa.pos289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import xa.pos289.models.Varian;

@Repository

public interface VarianRepo extends JpaRepository<Varian, Long> {
	@Query("From Varian WHERE Category_id = ?1")
	List<Varian> findByCategory_id(Long Category_id);

}
